/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package citeraffle;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListCell;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javax.swing.Timer;

/**
 *
 * @author User
 */
public class FXMLDocumentController implements Initializable {
    private Timer timer;
     @FXML
    private AnchorPane anchorpane;
    @FXML
    private Label label;
    @FXML
    private JFXListView<Label> winnerslistview;
    @FXML
    private JFXButton randomButton;
    @FXML
    private Text displayText;
    private List<Label> values,winners;
    private List<String> stringvalues,tempvalues;
    private String value,randomvalue;
    private int num,randomindex,count;
    private final Paint ripplerFill = Paint.valueOf("ffffff");
    private JFXButton b1;
    private JFXPopup popup;
    private JFXListView<JFXButton> popuplist;
    @FXML
    private JFXListView<Label> remaininglistview;
    @FXML
    void randomizeAction(ActionEvent event) {       
        try {           
            System.out.println(randomvalue);            
             count++;                                    
        if (count==2) {
            timer.stop();
            count=0;
            winners.add(new Label("         "+randomvalue));
            
            System.out.println("Index: "+ tempvalues.indexOf(randomvalue));
            
           values.remove(tempvalues.indexOf(randomvalue));
           tempvalues.remove(randomvalue);
           remaininglistview.getItems().clear();
           winnerslistview.getItems().clear();
           winnerslistview.getItems().addAll(winners);                
           remaininglistview.getItems().addAll(values);
            stringvalues.remove(stringvalues.remove(randomvalue));          
        }
        else{
          timer.start();          
        }            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        count=0;
        values = new ArrayList<>();
        tempvalues = new ArrayList<>();
        stringvalues = new ArrayList<>();
        winners = new ArrayList<>();
        initializePopup();
        for (int i = 0; i <= 20; i++) {
            if (i<10) {
               value = "00"+i;
            }
            else if(i<100 && i>=10){
                value="0"+i;           
            }
            else{
                value = String.valueOf(i);
            }
            stringvalues.add(value);
            tempvalues.add(value);
            values.add(new Label(value));
            
        }
        remaininglistview.getItems().addAll(values);
        try {
             timer = new Timer(60, (e) -> {         
             shuffleList(stringvalues);
             shuffleList(stringvalues);
             shuffleList(stringvalues);
             shuffleList(stringvalues);
             shuffleList(stringvalues);
             shuffleList(stringvalues);
             
            randomindex = getRandomIndex(0, stringvalues.size()-1);
            randomvalue = stringvalues.get(randomindex);
            
           displayText.setText(randomvalue);          
       });
        } catch (Exception e) {
            System.out.println(e);
        }
        
       
        
        winnerslistview.setCellFactory((param) -> {
             JFXListCell<Label> cell = new JFXListCell<>();
            
            cell.setOnMousePressed((event) -> {
                
            if (event.getButton().equals(MouseButton.SECONDARY)) {
                popup.show(cell, JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.LEFT , event.getX(),event.getY());
                b1.setOnAction((event1) -> {
                  System.out.println(winnerslistview.getSelectionModel().getSelectedItem().getText());
                  int index = winnerslistview.getSelectionModel().getSelectedIndex();
                  winners.remove(index);
                   winnerslistview.getItems().clear();
                    winnerslistview.getItems().addAll(winners);        
                  popup.hide();
                });
            }                     
            
        });
            
            return cell; //To change body of generated lambdas, choose Tools | Templates.
        });
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
    public void initializePopup(){
         popup = new JFXPopup(popuplist);
        Insets insets = new Insets(10);
        popuplist = new JFXListView<>();
         b1 = new JFXButton("Delete");       
        b1.setPadding(insets);       
        b1.setRipplerFill(ripplerFill);
        VBox box = new VBox(b1);

        popup.setPopupContent(box);      
    
    }
    

public List<String> shuffleList(List<String> values){
        for(int j=0;j<values.size();j++){
             num = (int)(Math.random() * values.size());   
             String temp = values.get(j);
               values.set(j, values.get(num));
               values.set(num, temp);      
        }     
     return values;
}
        public int getRandomIndex(int min,int max){
            int range = (max-min)+1;    
        num = (int)(Math.random() * range);   

        return num;
        }
    
}
